const HtmlWebpackPlugin = require('html-webpack-plugin'); //installed via npm
const ExtractTextPlugin = require("extract-text-webpack-plugin")
const webpack = require('webpack'); //to access built-in plugins
const path = require('path');

const config = {
  entry: './app/index.js',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist')
  },
  module: {
    rules: [
    {
      test: /\.tag$/,
      exclude: /node_modules/,
      enforce: 'pre',
      use: 'riot-tag-loader'
    },
    {
      test: /\.js$|\.tag$/,
      exclude: /node_modules/,
      use: 'babel-loader'
    }
    ]
  },
  resolve: {
    extensions: ['*', '.js', '.tag']
  },

  plugins: [
    new HtmlWebpackPlugin({
      template: 'app/index.html'
    }),

    new webpack.ProvidePlugin({
      riot: "riot/riot"
    })
  ]
};

module.exports = config;
