<item-list>

  <p show={loading}>loading...</p>

  <p hide={loading || items.length > 0}>
    No hay items
  </p>

  <ul class='list-group'>
    <li each={items} class='list-group-item'>
      { name }({ qty })
      <a href='#/items/{id}'>show</a>
      <a href='#/items/{id}/edit'>edit</a>
      <a onClick={parent.delete(id)} >x</a>
    </li>
  </ul>

  <script>
    import {
      fetchAllItems,
      deleteItem
    } from './../../actions/items.js'

    delete(id){
      return function(){
        opts.store.dispatch(deleteItem(id))
      }
    }

    this.loading = opts.store.getState().items.isFetching
    this.items = opts.store.getState().items.data

    opts.store.subscribe(function(){
      this.update({
        loading: opts.store.getState().items.isFetching,
        items: opts.store.getState().items.data
      })
    }.bind(this))

    this.on('mount', function() {
      opts.store.dispatch(fetchAllItems())
    })

  </script>

</item-list>
