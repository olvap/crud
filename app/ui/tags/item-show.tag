<item-show>

  <p show={loading}>loading...</p>

  <p>name: {item.name}</p>

  <a href='#/items'>back</a>

  <script>
    import { fetchItem } from './../../actions/items.js'

    this.item = opts.store.getState().items.current
    this.loading = opts.store.getState().items.isFetching

    this.on('mount', function(){
      opts.store.dispatch(fetchItem(opts.item_id))
    })

    opts.store.subscribe(function(){
      this.update({
        item: opts.store.getState().items.current,
        loading: opts.store.getState().items.isFetching
      })
    }.bind(this))
  </script>
</item-show>

