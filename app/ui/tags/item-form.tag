<item-form>
  <form onSubmit={submitItem} class='form-inline'>
    <input ref='name' class='form-control' />
    <button type="submit" class="btn btn-primary">Agregar item</button>
  </form>

  <script>
    import { createItem } from './../../actions/items.js'

    submitItem(e){
      e.preventDefault()
      opts.store.dispatch(
        createItem({
          name: this.refs.name.value,
          qty: 0
        })
      )
      this.refs.name.value = ''
    }
  </script>
</item-form>
