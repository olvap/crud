<item-edit-form>
  <form onSubmit={submitItem} class='form-inline'>
    <input ref='name' class='form-control'/>
    <button type="submit" class="btn btn-primary">Editar item</button>
  </form>

  <script>
    import { fetchItem, updateItem } from './../../actions/items.js'

    this.on('mount', function() {
      opts.store.dispatch(fetchItem(opts.item_id))
      this.refs.name.value = opts.store.getState().items.current.name
    })

    opts.store.subscribe(function(){
      this.refs.name.value = opts.store.getState().items.current.name
      this.update()
    }.bind(this))

    submitItem(e){
      e.preventDefault()
      opts.store.dispatch(
        updateItem({
          id: opts.item_id,
          name: this.refs.name.value,
          qty: 0
        })
      )
    }
  </script>
</item-edit-form>
