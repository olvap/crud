import store from './store.js'

import riot from 'riot'
import route from 'riot-route/tag'
import './ui/tags/item-list.tag'
import './ui/tags/item-app.tag'
import './ui/tags/item-form.tag'
import './ui/tags/item-edit-form.tag'
import './ui/tags/item-show.tag'

route('/items/new', function() {
  riot.mount('#main', 'item-form', { store: store })
})

route('/items', function() {
  riot.mount('#main', 'item-app', { store: store })
})

route('/items/*', function(id) {
  riot.mount('#main', 'item-show', { store: store, item_id: id })
})

route('/items/*/edit', function(id) {
  riot.mount('#main', 'item-edit-form', { store: store, item_id: id })
})

route.start(true);
window.store = store
