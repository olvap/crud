import reducer from '../items.js'

describe("articles reducer", () => {

  const initialState = [];

  describe('SET_ITEMS', () => {
    it('should set the collection', () => {
      const collection = [{ name: 'new data' }, {name: 'other'}]

      expect(reducer(
        initialState,
        { type: 'SET_ITEMS', data: collection })
      ).toEqual(collection);
    })
  })

  describe('ADD_ITEM', () => {

    it('should set the item', () => {
      const expected = [{ name: 'new data' }]

      expect(reducer(
        initialState,
        { type: 'ADD_ITEM', data: { name: 'new data' } })
      ).toEqual(expected);
    })
  })

  describe('DELETE_ITEM', () => {
    it('should remove item by given index', () => {
      const expected = [{id: 1, name: 'data'}]

      expect(reducer(
        [{id: 1, name: 'data'}, {id: 3, name: 'deleteme'}],
        { type: 'DELETE_ITEM', id: 3 })
      ).toEqual(expected);
    })
  })

  describe('EDIT_ITEM', () => {
    it('should edit item', () => {
      const expected = [{name: 'new name', id: 9}]

      expect(reducer(
        [{name: 'data', id: 9}],
        { type: 'EDIT_ITEM', data: { id: 9, name: 'new name' } })
      ).toEqual(expected);

    })
  })
})
