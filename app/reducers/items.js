import { combineReducers } from 'redux'

const isFetching = (state = true, action) => {
  switch(action.type) {
    case 'FETCH_ITEMS':
      return true
    case 'SET_ITEMS':
      return false
    case 'SET_ITEM':
      return false
    default:
      return state
  }
}

const current = (item = {}, action) => {
  switch (action.type) {
    case 'SET_ITEM':
      return action.data
    default:
      return item
  }
}

const data = (items = [], action) => {
  switch (action.type) {
    case 'SET_ITEMS':
      return action.data
    case 'ADD_ITEM':
      return items.concat(action.data);
    case 'EDIT_ITEM':
      const index = items.findIndex(item => item.id === action.data.id)
      return [
        ...items.slice(0, index),
        Object.assign(items[index], action.data),
        ...items.slice(index + 1)
      ]
    case 'DELETE_ITEM':
      return items.filter(item => item.id !== action.id)

    default:
      return items
  }
}

export default combineReducers({data, isFetching, current})
