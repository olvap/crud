export const createItem = (item) => (
  {
    type: 'API',
    url: 'items',
    method: 'POST',
    body: item,
    success: 'ADD_ITEM'
  }
)

export const fetchItem = (id) => (
  {
    type: 'API',
    url: 'items/' + id,
    id: id,
    method: 'GET',
    success: 'SET_ITEM',
    before_fetching: 'FETCH_ITEMS'
  }
)

export const deleteItem = (id) => (
  {
    type: 'API',
    url: 'items/' + id,
    id: id,
    method: 'DELETE',
    success: 'DELETE_ITEM'
  }
)

export const fetchAllItems = () => (
  {
    type: 'API',
    url: 'items',
    method: 'GET',
    success: 'SET_ITEMS',
    before_fetching: 'FETCH_ITEMS'
  }
)

export const updateItem = (item) => (
  {
    type: 'API',
    url: 'items/' + item.id,
    body: item,
    method: 'PATCH',
    success: 'EDIT_ITEM'
  }
)
